package com.zzf;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class ResourcesConfiguration extends WebMvcConfigurerAdapter {

	/**
	 * 
	 * 自定义静态资源文件路径（默认访问静态资源文件的路径是在/resources/static/目录下）
	 * 我们配置了静态资源的路径为/yuqiyu/9/resources/**，那么只要访问地址前缀是/yuqiyu/9/resources/，就会被自动转到项目根目录下的static文件夹内。
	 * 如：我们访问：127.0.0.1:8080//yuqiyu/9/resources/t.png就会被解析成127.0.0.1:8080/t.png。
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/yuqiyu/9/resources/**").addResourceLocations("classpath:/static/");
	}
}
